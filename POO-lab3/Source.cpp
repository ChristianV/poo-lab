#include "Header.h"

bool BigNumber::Set(int value)
{
	CharactersCount = 0;
	aux = 0;

	while (value)
	{
		aux = value % 10;
		Number[CharactersCount] = aux + '0';
		value = value / 10;
		CharactersCount++;
	}

	// debug
	for (int i = 0; i < CharactersCount; ++i)
		cout << Number[i];
	cout << endl;

	return true;
}

bool BigNumber::Set(const char * number)
{
	strcpy_s(Number, number);
	CharactersCount = strlen(number);

	// debug
	cout << number;

	return true;
}

int main()
{
	BigNumber a, b, c;

	/*a.Set(789);
	a.Set("899");*/

	a.Set(8);
	b.Set(5);
	c = a+b;

	//cout << c;

	/*BigNumber c(7), d(9), q;
	q = c + d;

	BigNumber q(c);*/

	system("pause");
	return 0;
}